#pragma once
#include <QDebug>
#include <QString>

#include <qt_windows.h>
#include <winsvc.h>

class WindowsService {

public:
    WindowsService(
        const QString serviceName,
        const QString displayName,
        const QString appPath,
        const bool autostart = true);

    ~WindowsService();
    bool create();
    bool remove();

    bool pause();
    bool resume();

    bool stop();
    bool start();

private:
    SC_HANDLE m_scManager = nullptr;

    LPCTSTR m_serviceName = nullptr;
    LPCTSTR m_displayName = nullptr;
    LPCTSTR m_appPath = nullptr;
    bool m_autostart = false;
};
