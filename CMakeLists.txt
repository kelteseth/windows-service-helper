cmake_minimum_required(VERSION 3.14)

project(WindowsServiceHelper LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Core REQUIRED)

set(SOURCE main.cpp windowsservice.cpp)
set(HEADER windowsservice.h)

add_executable(${PROJECT_NAME} ${SOURCE} ${HEADER})

target_link_libraries(${PROJECT_NAME} PRIVATE Qt5::Core )

# Start as admin
set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS "/MANIFESTUAC:\"level='requireAdministrator' uiAccess='false'\" /SUBSYSTEM:WINDOWS")

# Disable console window on Windows
# https://stackoverflow.com/questions/8249028/how-do-i-keep-my-qt-c-program-from-opening-a-console-in-windows
set_property(TARGET ${PROJECT_NAME} PROPERTY WIN32_EXECUTABLE true)

