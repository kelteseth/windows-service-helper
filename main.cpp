#include "windowsservice.h"
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QString>

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationName("Windows Service Helper");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption typeOption({ "t", "type" }, "Create,Remove,Start,Stop,Pause,Resume");
    parser.addOption(typeOption);

    QCommandLineOption nameOption({ "sn", "serviceName" }, "");
    parser.addOption(nameOption);

    QCommandLineOption displayNameOption({ "dn", "displayName" }, "");
    parser.addOption(displayNameOption);

    QCommandLineOption pathOption({ "p", "path" }, "");
    parser.addOption(pathOption);

    QCommandLineOption autostartOption({ "a", "autostartOption" }, "");
    parser.addOption(autostartOption);

    parser.process(app);

    if (!(parser.isSet(typeOption) && parser.isSet(nameOption) && parser.isSet(displayNameOption) && parser.isSet(pathOption) && parser.isSet(autostartOption))) {
        qWarning() << "missing argument!";
        return -1;
    }

    WindowsService windowService(
        parser.value(nameOption),
        parser.value(displayNameOption),
        parser.value(pathOption),
        QVariant(parser.value(autostartOption)).toBool());

    if (parser.value(typeOption) == "Create") {
        return windowService.create() ? 0 : -1;
    }
    if (parser.value(typeOption) == "Remove") {
        return windowService.stop() ? 0 : -1;
    }

    if (parser.value(typeOption) == "Start") {
        return windowService.start() ? 0 : -1;
    }

    if (parser.value(typeOption) == "Stop") {
        return windowService.stop() ? 0 : -1;
    }

    if (parser.value(typeOption) == "Pause") {
        return windowService.pause() ? 0 : -1;
    }

    if (parser.value(typeOption) == "Resume") {
        return windowService.resume() ? 0 : -1;
    }
}
