#include "windowsservice.h"

WindowsService::WindowsService(const QString serviceName, const QString displayName, const QString appPath, const bool autostart)
{
    m_serviceName = (LPCTSTR)serviceName.utf16();
    m_displayName = (LPCTSTR)displayName.utf16();
    m_appPath = (LPCTSTR)appPath.utf16();
    m_autostart = autostart;

    m_scManager = OpenSCManager(
        nullptr, // local computer
        nullptr, // ServicesActive database
        SC_MANAGER_ALL_ACCESS); // full access rights

    if (!m_scManager) {
        qWarning() << "OpenSCManager failed " << GetLastError();
    }
}

WindowsService::~WindowsService()
{
    CloseServiceHandle(m_scManager);
}

bool WindowsService::create()
{

    SC_HANDLE tmpHandle = CreateService(
        m_scManager,
        m_serviceName,
        m_displayName,
        SERVICE_ALL_ACCESS,
        SERVICE_WIN32_OWN_PROCESS,
        m_autostart ? SERVICE_AUTO_START : SERVICE_DEMAND_START,
        SERVICE_ERROR_NORMAL,
        m_appPath,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr);

    if (tmpHandle) {
        qWarning() << tmpHandle;
        return true;
    }
    CloseServiceHandle(tmpHandle);

    return false;
}

bool WindowsService::remove()
{
    SC_HANDLE tmpHandle;
    tmpHandle = OpenService(m_scManager, m_serviceName, SERVICE_ALL_ACCESS);
    if (tmpHandle) {
        if (DeleteService(tmpHandle)) {
            return true;
        }
    }
    CloseServiceHandle(tmpHandle);

    return false;
}

bool WindowsService::pause()
{
    SC_HANDLE tmpHandle;
    tmpHandle = OpenService(m_scManager, m_serviceName, SERVICE_ALL_ACCESS);
    if (tmpHandle) {
        SERVICE_STATUS m_SERVICE_STATUS;

        if (ControlService(tmpHandle, SERVICE_CONTROL_PAUSE, &m_SERVICE_STATUS)) {
            return true;
        }
    }
    CloseServiceHandle(tmpHandle);

    return false;
}

bool WindowsService::resume()
{
    SC_HANDLE tmpHandle;
    tmpHandle = OpenService(m_scManager, m_serviceName, SERVICE_ALL_ACCESS);
    if (tmpHandle) {
        SERVICE_STATUS m_SERVICE_STATUS;

        if (ControlService(tmpHandle, SERVICE_CONTROL_CONTINUE, &m_SERVICE_STATUS)) {
            return true;
        }
    }
    CloseServiceHandle(tmpHandle);

    return false;
}

bool WindowsService::stop()
{
    SC_HANDLE tmpHandle;
    tmpHandle = OpenService(m_scManager, m_serviceName, SERVICE_ALL_ACCESS);
    if (tmpHandle) {
        SERVICE_STATUS m_SERVICE_STATUS;

        if (ControlService(tmpHandle, SERVICE_CONTROL_STOP, &m_SERVICE_STATUS)) {
            return true;
        }
    }
    CloseServiceHandle(tmpHandle);

    return false;
}

bool WindowsService::start()
{
    SC_HANDLE tmpHandle;
    tmpHandle = OpenService(m_scManager, m_serviceName, SERVICE_ALL_ACCESS);
    if (tmpHandle) {
        if (StartService(tmpHandle, 0, nullptr)) {
            return true;
        }
    }
    CloseServiceHandle(tmpHandle);

    return false;
}
