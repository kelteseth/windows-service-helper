### Windows Service Helper
This single app lets you manage Windows Services:
* Create
* Remove
* Start
* Stop
* Pause
* Resume

The app must be called via command line parameter:

* WindowServiceHelper.exe -create -serviceName "My Service Name" -displayName "My Display Name" -appPath "c:\\mypath\\my.exe" -autostart true 

### Development setup
Open this project with any Qt version newer than 5.15. Older version should also work, but are not tested!
